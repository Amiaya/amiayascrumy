from django.apps import AppConfig


class AmiayascrumyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'amiayascrumy'
